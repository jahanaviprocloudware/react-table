import logo from './logo.svg';
import './App.css';
import BasicFilterDemo from './ReactTable';

function App() {
  return (
    <div className="App">
      <BasicFilterDemo/>
    </div>
  );
}

export default App;
